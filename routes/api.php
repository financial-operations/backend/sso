<?php

use Illuminate\Http\Response;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group( function () {

    Route::post('checktoken', 'TokenController@check')->name('token.check');

    Route::get('user', 'UserController@index')->name('user.index');
    Route::get('user/{user_id}', 'UserController@show')->name('user.show');

    Route::middleware('api.admin')->group( function () {
        Route::post('user', 'UserController@store')->name('user.store');
        Route::put('user/{user_id}', 'UserController@update')->name('user.update');
        Route::delete('user', 'UserController@destroy')->name('user.destroy');
    });
});

Route::post('newtoken', 'TokenController@new')->middleware('api.user')->name('token.new');

Route::get('accessdeny', function () {
    return response()->json(['error' => 'Invalid token'], Response::HTTP_UNAUTHORIZED);
})->name('access.deny');