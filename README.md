# Microservice SSO - Single Sign On

Microsserviço de autenticação responsável pela gerência (cadastro, atualização e autorização dos usuários.

### Settings:
- [Laravel Framework - Versão 5.8.38](https://laravel.com/docs/5.8)
- [PHP - Versão 7.2.31](https://www.php.net/downloads.php)

### Requirements:
- [Laravel Framework - v.5.8+](https://laravel.com/docs/5.8/installation#server-requirements)
- [Laravel Passaport - v.5.8+](https://laravel.com/docs/5.8/passport)


- ### POST http://localhost:porta/v1/token

- #### Request:
```json
    {
        "email": "bill.gates@outlook.com",
        "password": "password"
    }
```

- #### Response:
```json
        {
            "user": {
                "id": 1,
                "name": "Bill Gates",
                "type_user": "Admin",
                "type_document": "CPF",
                "document_number": "99988877714",
                "email": "bill.gates@outlook.com",
                "created_at": "24/08/2020 06:05:26",
                "updated_at": "24/08/2020 06:05:26"
            },
            "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzII04U_eUEcpvjI05HTd0IHXgwYWvJMya9FGOVDpQyy1w_JFBxHykNnoi5iyUJfhaTF8W_FFn4CXEb44EFoU_MkY2QG1en08VvQSpjawnsQXEZljmW7uvewgwePMXgyHFRJ3ZcJrT_bSmNInh3QtCkLxcrUGQvsvztn8rYOle2QAV3QPx0TWNgPaMz9rrHtxq3Q1laePexwriD21hkrDJkdC-4SdkpGFkZhhQnM0I4e6dFReE1bPLc_pAaOu0eKquiKcukXIKCTpyZtluUCveelMXiHzAqbJ89ncIo1_yqqQKN-dl7RZM5aZNW3JCyikOTNLsUh8JL7ZYEg_h9feA_wOTNA0JnCVOYdBnwepR1RgVl3q50yGU_bDVQ3NtwAaBUcjumIDZubBAjjWbseMoYkAYY2kEL7g2Q4Wds7o40",
            "expires_at": "2020-09-01 15:55:16"
        }
```

- ### POST  http://localhost:porta/v1/user
    
- #### Header:
```json
        "header": {
            "Content-Type": "application/json",
            "Authorization": "Bearer token (passar token aqui)",
        }
```
    
- #### Request:
```json
        {
            "sysadmin": {
                "email": "bill.gates@outlook.com"
            },
            "user": {
                "profile_type": "Comum",
                "document": "CPF",
                "document_number": "69928716685",
                "name": "Steve Jobs",
                "email": "steve.jobs@apple.com",
                "password": "password",
                "is_active": 1
            }
        }
```

- #### Response:
```json
    true
```

- ### GET  http://localhost:porta/v1/user
    
- #### Header:
```json
        "header": {
            "Content-Type": "application/json",
            "Authorization": "Bearer token (passar token aqui)",
        }
```

- #### Response:
```json
        {
            "id": 1,
            "name": "Elohin Modas",
            "type_user": "Logista",
            "type_document": "CNPJ",
            "document_number": "88547891580001",
            "email": "elohinmodas@mail.com",
            "created_at": "31/08/2020 13:01:59",
            "updated_at": "31/08/2020 13:01:59"
        },
        {
            "id": 2,
            "name": "Padaria Pão Certo",
            "type_user": "Logista",
            "type_document": "CNPJ",
            "document_number": "98513547890001",
            "email": "paocerto@mail.com",
            "created_at": "31/08/2020 13:10:14",
            "updated_at": "31/08/2020 13:10:14"
        },
        {
            "id": 3,
            "name": "Ana Beatriz Lopes",
            "type_user": "Comum",
            "type_document": "CPF",
            "document_number": "65428716685",
            "email": "analopes@mail.com",
            "created_at": "31/08/2020 16:21:49",
            "updated_at": "31/08/2020 16:21:49"
        }
```

- ### GET  http://localhost:porta/v1/user/id
    
- #### Header:
```json
        "header": {
            "Content-Type": "application/json",
            "Authorization": "Bearer token (passar token aqui)",
        }
```
    
- #### Response:
```json
        {
            "id": 1,
            "name": "Elohin Modas",
            "type_user": "Logista",
            "type_document": "CNPJ",
            "document_number": "88547891580001",
            "email": "elohinmodas@mail.com",
            "created_at": "31/08/2020 13:01:59",
            "updated_at": "31/08/2020 13:01:59"
        }
```

### Installation and Configuration:

- Clone projeto
        
        `git@gitlab.com:financial-operations/backend/sso.git`

- Imagem docker
        
        `registry.gitlab.com/financial-operations/backend/sso:latest`
        
### Executar somente o microservice Wallet.

- 1) Fazer o clone do projeto:

        `git@gitlab.com:financial-operations/backend/sso.git`

- 2) Acessar o diretório sso e executar os seguintes comandos e/ou configurações:
        

        `configurar o arquivo .env (ver modelo no arquivo .env.example`

        `Instalar dependências do projeto com: `composer install`
        
        `php artisan passport:install`

- 3) Subir a aplicação em um container docker (troque o nome home_user pelo diretório do projeto):
        
        `docker container run -dit --memory="256m" --name orq-sso -h ms-sso -v ~/home_user/sso:/var/www -p 9001:80 registry.gitlab.com/financial-operations/backend/sso:latest
        && docker container exec orq-sso service apache2 start && docker container exec orq-sso service apache2 status`


