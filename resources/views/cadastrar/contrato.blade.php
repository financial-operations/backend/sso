@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registrar Contrato</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registrarcontrato') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('UF') }}</label>

                            <div class="col-md-6">
                                <select name="uf" id="uf" class="form-control">
                                    <option value=""></option>
                                    @foreach($ufs as $uf)
                                        <option value="{{ $uf->id }}">{{ $uf->uf }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Fornecedor') }}</label>

                            <div class="col-md-6">
                                <select name="fornecedor" id="fornecedor" class="form-control">
                                    <option value=""></option>
                                    @foreach($fornecedores as $fornecedor)
                                        <option value="{{ $fornecedor->id }}">{{ $fornecedor->fornecedor }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de Serviço') }}</label>

                            <div class="col-md-6">
                                <select name="tipo_servico" id="tipo_servico" class="form-control">
                                    <option value=""></option>
                                    @foreach($tipo_servicos as $tiposervico)
                                        <option value="{{ $tiposervico->id }}">{{ $tiposervico->tipo }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Projetos SSP') }}</label>

                            <div class="col-md-6">
                                <select name="projeto_ssp" id="projeto_ssp" class="form-control">
                                    <option value=""></option>
                                    @foreach($projetos as $projeto)
                                        <option value="{{ $projeto->id }}">{{ $projeto->nome_projeto }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
