<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('profile_type_id');
            $table->unsignedBigInteger('document_id');
            $table->string('name');
            $table->string('document_number');
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->boolean('is_active')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('profile_type_id')
                    ->references('id')
                    ->on('profile_type');
            
            $table->foreign('document_id')
                    ->references('id')
                    ->on('document');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
