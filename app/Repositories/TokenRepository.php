<?php

namespace App\Repositories;

use Illuminate\Http\Response;

use App\Repositories\Contracts\TokenRepositoryInterface;

class TokenRepository implements TokenRepositoryInterface
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function generateToken()
    {
        $objToken = \Auth::user()->createToken(\Auth::user()->document_number.'-'.now());
        
        return [
            'user' => $this->userRepository->getById(\Auth::user()->id),
            'access_token' => $objToken->accessToken,
            'expires_at' => $objToken->token->expires_at->toDateTimeString('second')
        ];
    }
}