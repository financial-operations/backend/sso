<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

use App\Repositories\Contracts\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    public function getAll()
    {
        return  DB::table('users as u')
                    ->join('profile_type as pt', 'u.profile_type_id', '=', 'pt.id')
                    ->join('document as d', 'u.document_id', '=', 'd.id')
                    ->where('u.is_active', '=', true)
                    ->select(
                        'u.id',
                        'u.name',
                        'pt.type as type_user',
                        'd.type as type_document',
                        'u.document_number',
                        'u.email',
                        DB::raw('DATE_FORMAT(u.created_at, "%d/%m/%Y %H:%i:%S") as created_at'),
                        DB::raw('DATE_FORMAT(u.updated_at, "%d/%m/%Y %H:%i:%S") as updated_at')
                    )
                    ->get();       
    }


    public function save(array $dataProfile)
    {
        return DB::table('users')->insert($dataProfile);
    }

    public function update(array $dataProfile)
    {
        return DB::table('users')->update($dataProfile);
    }

    public function getInactives()
    {
        return  DB::table('users as u')
                    ->join('profile_type as pt', 'u.profile_type_id', '=', 'pt.id')
                    ->join('document as d', 'u.document_id', '=', 'd.id')
                    ->where('u.is_active', '=', false)
                    ->select(
                        'u.id',
                        'u.name',
                        'pt.type as type_user',
                        'd.type as type_document',
                        'u.document_number',
                        'u.email',
                        DB::raw('DATE_FORMAT(u.created_at, "%d/%m/%Y %H:%i:%S") as created_at'),
                        DB::raw('DATE_FORMAT(u.updated_at, "%d/%m/%Y %H:%i:%S") as updated_at')
                    )
                    ->get();
    }

    public function getById($user_id)
    {
        return  DB::table('users as u')
                    ->join('profile_type as pt', 'u.profile_type_id', '=', 'pt.id')
                    ->join('document as d', 'u.document_id', '=', 'd.id')
                    ->where('u.id', '=', $user_id)
                    ->where('u.is_active', '=', true)
                    ->select(
                        'u.id',
                        'u.name',
                        'pt.type as type_user',
                        'd.type as type_document',
                        'u.document_number',
                        'u.email',
                        DB::raw('DATE_FORMAT(u.created_at, "%d/%m/%Y %H:%i:%S") as created_at'),
                        DB::raw('DATE_FORMAT(u.updated_at, "%d/%m/%Y %H:%i:%S") as updated_at')
                    )
                    ->first();
    }

    public function getByEmail($email_address)
    {
        return  DB::table('users as u')
                    ->join('profile_type as pt', 'u.profile_type_id', '=', 'pt.id')
                    ->join('document as d', 'u.document_id', '=', 'd.id')
                    ->where('u.email', '=', $email_address)
                    ->where('u.is_active', '=', true)
                    ->select(
                        'u.id',
                        'u.name',
                        'pt.type as type_user',
                        'd.type as type_document',
                        'u.document_number',
                        'u.email',
                        DB::raw('DATE_FORMAT(u.created_at, "%d/%m/%Y %H:%i:%S") as created_at'),
                        DB::raw('DATE_FORMAT(u.updated_at, "%d/%m/%Y %H:%i:%S") as updated_at')
                    )
                    ->first();
    }

    public function getByDocumentNumber($document_number)
    {
        return  DB::table('users as u')
                    ->join('profile_type as pt', 'u.profile_type_id', '=', 'pt.id')
                    ->join('document as d', 'u.document_id', '=', 'd.id')
                    ->where('u.document_number', '=', $document_number)
                    ->where('u.is_active', '=', true)
                    ->select(
                        'u.id',
                        'u.name',
                        'pt.type as type_user',
                        'd.type as type_document',
                        'u.document_number',
                        'u.email',
                        DB::raw('DATE_FORMAT(u.created_at, "%d/%m/%Y %H:%i:%S") as created_at'),
                        DB::raw('DATE_FORMAT(u.updated_at, "%d/%m/%Y %H:%i:%S") as updated_at')
                    )
                    ->first();
    }
}