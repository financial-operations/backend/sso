<?php

namespace App\Repositories\Contracts;

interface UserRepositoryInterface
{
    public function getAll();
    
    public function save(array $dataProfile);

    public function getById($user_id);

    public function update(array $dataProfile);

    public function getInactives();

    public function getByEmail($email_address);

    public function getByDocumentNumber($document_number);
}