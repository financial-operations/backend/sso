<?php

namespace App\Repositories\Contracts;

interface TokenRepositoryInterface
{
    public function generateToken();
}