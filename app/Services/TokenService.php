<?php

namespace App\Services;

use App\Repositories\Contracts\TokenRepositoryInterface;

class TokenService
{
    private $tokenRepository;

    public function __construct(TokenRepositoryInterface $tokenRepository)
    {
        $this->tokenRepository = $tokenRepository;
    }

    public function newToken($requestDataToken)
    {
        return $this->tokenRepository->generateToken();
    }
}