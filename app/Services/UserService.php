<?php

namespace App\Services;

use Illuminate\Http\Response;

use App\Repositories\Contracts\UserRepositoryInterface;

class UserService
{
    private const USER_PROFILE = [
        'Comum' => 1,
        'Logista' => 2,
        'CPF' => 1,
        'CNPJ' => 2
    ];

    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAllUsers()
    {
        return $this->userRepository->getAll();
    }
    
    public function createNewUser($requestDataProfile)
    {
        if ($userProfile = $this->getProfileUserByEmail($requestDataProfile->user['email'])) {
            if ($userProfile && $userProfile->email == $requestDataProfile->user['email']) {
                return response()->json(['error' => 'Email '. $userProfile->email.' já cadastrado para o usuário.'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        if ($userProfile = $this->getProfileUserByDocumentNumber($requestDataProfile->user['document_number'])) {
            if ($userProfile && $userProfile->document_number == $requestDataProfile->user['document_number']) {
                return response()->json(['error' => $userProfile->type_document .' de nº '.$userProfile->document_number .' já cadastrado para este usuário.'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }
        
        return $this->userRepository->save($this->formatProfileUser($requestDataProfile->user));
    }

    public function getProfileUserById($user_id)
    {
        return $this->userRepository->getById($user_id);
    }

/*     public function updateProfileUser($requestDataProfile, $user_id)
    {
        if ($userProfile = $this->getProfileUserByEmail($requestDataProfile->email)) {
            if (empty($userProfile)) {
                return response()->json(['error' => 'Usuário não possui cadastro em nossos sistemas!'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }


        $requestDataProfile->password = Hash::make($requestDataProfile->password);

        return $this->userRepository->update($requestDataProfile);
    } */

    public function getAllInactives()
    {
        return $this->userRepository->getInactives();
    }
   
    public function getProfileUserByEmail(string $email_address)
    {
        return $this->userRepository->getByEmail($email_address);
    }
    
    public function getProfileUserByDocumentNumber(string $document_number)
    {
        return $this->userRepository->getByDocumentNumber($document_number);
    }

    public function formatProfileUser($user)
    {
        return [
            "profile_type_id"   => $user['profile_type'] == 'Comum' ? self::USER_PROFILE['Comum'] : self::USER_PROFILE['Logista'],
            "document_id"       => $user['document'] == 'CPF' ? self::USER_PROFILE['CPF'] : self::USER_PROFILE['CNPJ'],
            "name"              => $user['name'],
            "document_number"   => $user['document_number'],
            "email"             => $user['email'],
            "password"          => \Hash::make($user['password']),
            "is_active"         => $user['is_active'],
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        ];
    }
}