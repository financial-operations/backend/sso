<?php

namespace App\Http\Middleware;

use Closure;

class UserAuthorization
{
    private const PROFILE_TYPE = [
        'is_active' => 1
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $credentials = ['email' => $request->email, 'password' => $request->password, 'is_active' => self::PROFILE_TYPE['is_active']];
        if (!\Auth::attempt($credentials)) {
            return route('access.deny');
        }
  
        return $next($request);
    }
}
    