<?php

namespace App\Http\Middleware;

use Closure;

class SysAdmin
{
    private const PROFILE_TYPE = [
        'comum' => 1,
        'logista' => 2,
        'admin' => 3,
        'active' => 1,
        'inactive' => 0
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $credentials = ['email' => $request->sysadmin['email'], 'is_active' => self::PROFILE_TYPE['active'], 'profile_type_id' => self::PROFILE_TYPE['admin']];
        if (!\Auth::check($credentials)) {
            return route('access.deny');
        }
        
        return $next($request);
    }
}
