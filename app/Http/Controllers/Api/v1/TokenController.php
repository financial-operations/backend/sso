<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Controllers\Controller;

use App\Services\TokenService;

class TokenController extends Controller
{
    private $tokenService;

    public function __construct(TokenService $tokenService)
    {
        $this->tokenService = $tokenService;
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function new(Request $request)
    {
        try {
            return response()->json($this->tokenService->newToken($request));
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Falha ao tentar gerar o token. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function check(Request $request)
    {
        try {
            return response()->json(['message' => 'Access allowed'], Response::HTTP_ACCEPTED);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Falha ao tentar verificar a autenticidade do token. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
