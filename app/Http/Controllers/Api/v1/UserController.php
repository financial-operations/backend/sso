<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Services\UserService;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return response()->json($this->userService->getAllUsers(), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Falha ao tentar recuperar os usuários. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        try {
            return response()->json($this->userService->getProfileUserById($user_id), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Falha ao tentar recuperar os dados do usuário. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        try {
            return response()->json($this->userService->createNewUser($request), Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Falha ao tentar salvar o usuário. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
